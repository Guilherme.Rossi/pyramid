<html>
<form action="#" method="get">
    <input type="number" id="numero" placeholder="linhas">
    <button type="submit" name="enviar" onclick="gerar()"> Gerar </button>
</form>
<span id="result"> </span>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
axios({
    method:'get',
    url:'funcao.php',
    responseType:'stream'
})

function gerar(){
    var numero = document.getElementById("numero").value;
    console.log(numero);
    axios.get(`funcao.php?numero=${numero}`)
      .then(function (response) {
        var resposta = response.data;
        document.getElementById("result").innerHTML = resposta;
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        
      });
}
</script>
</html>